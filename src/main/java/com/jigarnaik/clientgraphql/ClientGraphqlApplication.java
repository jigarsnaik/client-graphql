package com.jigarnaik.clientgraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientGraphqlApplication.class, args);
	}

}
