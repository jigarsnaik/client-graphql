package com.jigarnaik.clientgraphql.exception;

import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

public class ClientNotFoundException extends RuntimeException implements GraphQLError {

    public ClientNotFoundException(String message) {
        super(message);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorClassification getErrorType() {
        return null;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
