package com.jigarnaik.clientgraphql.exception;

import graphql.GraphQLException;
import graphql.kickstart.spring.error.ThrowableGraphQLError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
@Slf4j
public class GraphqlExceptionHandler {

    @ExceptionHandler(ClientNotFoundException.class)
    public ClientNotFoundException handle(ClientNotFoundException e) {
        return new ClientNotFoundException(e.getMessage());
    }

    @ExceptionHandler(GraphQLException.class)
    public ThrowableGraphQLError handle(GraphQLException e) {
        return new ThrowableGraphQLError(e, "Something went wrong");
    }
}
