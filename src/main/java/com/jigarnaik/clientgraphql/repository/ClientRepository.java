package com.jigarnaik.clientgraphql.repository;

import com.jigarnaik.clientgraphql.domain.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

}
