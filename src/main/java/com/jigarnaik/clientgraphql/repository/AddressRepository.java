package com.jigarnaik.clientgraphql.repository;

import com.jigarnaik.clientgraphql.domain.Address;
import com.jigarnaik.clientgraphql.domain.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByClient(Client client);
}
