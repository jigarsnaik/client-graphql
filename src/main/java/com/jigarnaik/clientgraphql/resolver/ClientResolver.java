package com.jigarnaik.clientgraphql.resolver;

import com.jigarnaik.clientgraphql.domain.Client;
import com.jigarnaik.clientgraphql.service.ClientService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ClientResolver implements GraphQLQueryResolver {

    private ClientService clientService;

    public Client client(Long id) {
        log.info("retrieving client with id : {}", id);
        return clientService.getClient(id);
    }
}
