package com.jigarnaik.clientgraphql.resolver;

import com.jigarnaik.clientgraphql.domain.Address;
import com.jigarnaik.clientgraphql.domain.Client;
import com.jigarnaik.clientgraphql.service.AddressService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class AddressResolver implements GraphQLResolver<Client> {

    private AddressService addressService;

    public List<Address> address(Client client) {
        log.info("Retrieving address for client : {}", client.getId());
        return addressService.getAddress(client);
    }

}
