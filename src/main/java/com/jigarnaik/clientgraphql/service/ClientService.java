package com.jigarnaik.clientgraphql.service;

import com.jigarnaik.clientgraphql.domain.Client;
import com.jigarnaik.clientgraphql.exception.ClientNotFoundException;
import com.jigarnaik.clientgraphql.repository.ClientRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class ClientService {

    private ClientRepository clientRepository;

    public Client getClient(Long id) {
        return clientRepository.findById(id).orElseThrow(() -> new ClientNotFoundException("Client not found matching id " + id));
    }
}
