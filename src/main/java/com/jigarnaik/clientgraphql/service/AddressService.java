package com.jigarnaik.clientgraphql.service;

import com.jigarnaik.clientgraphql.domain.Address;
import com.jigarnaik.clientgraphql.domain.Client;
import com.jigarnaik.clientgraphql.repository.AddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class AddressService {

    private AddressRepository addressRepository;

    public List<Address> getAddress(Client client) {
        return addressRepository.findByClient(client);
    }
}
