# client-graphql -  Project Status - Work in Progress

Sample GraphQL service for maintaining client and child entities using graphql
- Project uses GraphQL
- PostgreSQL
- Docker 
- Liquibase

#### Playground URL http://localhost:8080/playground
#### Voyager URL http://localhost:8080/voyager
#### Build the project `mvn clean package`
#### Build the image and bring up the containers for app and db `docker-compose up`
#### Bring down the app and db `docker-compose down`
#### Rebuild and start container for app `docker rmi client-graphql \ docker-compose up`

